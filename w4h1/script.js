"using strict"; //don't allow global variables - safety feature: can't be lazy with variables

//self involking function - called immediately on load
(function(){
    //var vs let vs const
    let x = 1;

    if (x === 1){
        let x = 2;
        console.log("inside scope");
        console.log(`x = ${x}`); //variable x = value of x
        
    }
    console.log("outside scope");
    console.log(`x = ${x}`); //variable x = value of x
    
    /********/

    var y = 1;
    if (y === 1){
        var y = 2; //overrode line 19
        console.log("inside scope");
        console.log(`y = ${y}`);
    }
    console.log("outside scope");
    console.log(`y = ${y}`);

    /********/

    const c = 0;

    console.log(c);

    try { 
        c = 3;
    }catch(error){
        condole.error(error);
    }


    //Truthy / Falsy

    console.log("false == 'false'", false == 'false'); //false
    console.log("false === 'false'", false === 'false'); //false

    console.log("false == '0'", false == '0'); //true
    console.log("false === '0'", false === '0'); //false

    console.log("' \\t\\r\\n ' == 0", ' \t\r\n ' == 0); //true
    console.log("' \\t\\r\\n ' === 0", ' \t\r\n ' === 0); //false

    let array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    for (let index = 0; index < array.length; index ++){
        const element =  array[index];
        console.log(element);
    }

    //guaranteed to have order.. cannot break?
    array.forEach(function(value, index){
        console.log("inside forEach loop. value: ", value, "at index: ", index);
    })



})(); 